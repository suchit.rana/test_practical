from django.db import models
from django.utils import timezone
# Create your models here.

class userdetailsSoftDelete(models.Manager):
    def get_queryset(self):
        return  super(userdetailsSoftDelete, self).get_queryset().filter(deleted_at__isnull=True).order_by('-id')


class users(models.Model):
    name = models.CharField(max_length=255, default=None, blank=True, null=True)
    address = models.CharField(max_length=255, default=None, blank=True, null=True)
    ip_address = models.CharField(max_length=200,blank=True, null=True)
    role_id = models.IntegerField(default=1, blank=True, null=True)
    email = models.EmailField(max_length=255, default=None, blank=True, null=True, unique=True)
    email2 = models.EmailField(max_length=255, default=None, blank=True, null=True)
    password = models.CharField(max_length=255, default=None, blank=True, null=True)
    created_at = models.DateTimeField(editable=False, default=None)
    updated_at = models.DateTimeField(default=None)
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = userdetailsSoftDelete()
    original_objects = models.Manager()


    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(users, self).save(*args, **kwargs)

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()


class post(models.Model):
    image = models.ImageField(upload_to='post/', default=None, blank=True, null=True)
    userid = models.IntegerField(default=None, blank=True, null=True)
    created_at = models.DateTimeField(editable=False, default=None)
    updated_at = models.DateTimeField(default=None)
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = userdetailsSoftDelete()
    original_objects = models.Manager()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(post, self).save(*args, **kwargs)

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()


    @property
    def getUserName(self):
        data =  users.objects.values('name').filter(id=self.userid)
        return data[0]['name']

class comments(models.Model):
    comment = models.CharField(max_length=1055, default=None, blank=True, null=True)
    userid = models.IntegerField(default=None, blank=True, null=True)
    postid = models.IntegerField(default=None, blank=True, null=True)

    created_at = models.DateTimeField(editable=False, default=None)
    updated_at = models.DateTimeField(default=None)
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = userdetailsSoftDelete()
    original_objects = models.Manager()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(comments, self).save(*args, **kwargs)

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()



    @property
    def getUserName(self):
        data =  users.objects.values('name').filter(id=self.userid)
        return data[0]['name']