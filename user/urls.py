from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from . import views



urlpatterns = [
    path('', views.login),
    path("register", views.register, name='register'),
    path("dashboard", views.dashboard, name='dashboard'),
    path("viewpost/<int:id>", views.viewpost, name='viewpost'),
    path("userpost", views.userpost, name='userpost'),
    # path("index", views.index, name='index'),
    path("add", views.addpost, name="add"),
    # path("edit/<int:id>", views.edituser, name="edit"),
    # path("edit", views.edituser, name="edit"),
    # path('delete/<int:id>/', views.delete, name='delete'),
    # path('logout', views.logout, name='logout'),
    # path('sentmsg', views.sentmsg, name='sentmsg'),
#
]
urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)