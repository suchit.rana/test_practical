from django.shortcuts import render, redirect
from .models import users as User
from .models import post,comments
from django.contrib.auth.hashers import make_password, check_password
from datetime import datetime
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from getmac import getmac
from django.contrib.auth import authenticate, login


####################################################
# from django.contrib.gis.utils import GeoIP
# for store location using ip address
####################################################

# Create your views here.
import re


def login(request, user=None):
    if request.method == 'POST':
        email = request.POST.get('uname')
        password = request.POST.get('pass')
        if email == "" or password == "":
            context = {'error': True, 'message': 'Email Or Password Are Required ..!'}
            return render(request, 'login.html', context)
        regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
        if re.search(regex, email):
            post = User.objects.filter(email=email).first()
            if post:
                if check_password(password, post.password):
                    userdata = {'userid':post.id,'name': post.name, 'email': post.email,'role':post.role_id}
                    request.session['username'] = userdata
                    return redirect('dashboard')
                else:
                    if request.session.get('count', 0) == 0:
                        request.session['count'] = 1
                    else:
                        request.session['count'] += 1

                        if request.session['count'] == 3:
                            context = {'error': True, 'message': 'your Account has been blocked by sometimes'}
                            return render(request, 'login.html', context)

                        context = {'error': True, 'message': 'Email Or Password Wrong ..! Please Try Again.'}
                        return render(request, 'login.html', context)
            else:
                print("ixxxxxxxxxxn")
                context = {'error': True, 'message': 'Email Or Password Wrong ..! Please Try Again.'}
                return render(request, 'login.html', context)
        else:
            context = {'error': True, 'message': 'Email Or Password Wrong ..! Please Try Again.'}
            return render(request, 'login.html', context)
    return render(request, 'login.html')


def register(request, user=None):
    response = {}
    if request.method == 'POST':
        userssubmit = User()
        userssubmit.name = request.POST.get('name')
        userssubmit.email = request.POST.get('email')
        userssubmit.email2 = request.POST.get('aemail')
        print(request.POST.get('password'),"deeeeeeeeeeeeeee")
        userssubmit.password = make_password(request.POST.get('password'))
        userssubmit.ip_address = getmac.get_mac_address(ip=get_client_ip(request), network_request=True)
        userssubmit.save()
        response['message'] = "DATA SAVED SUCCESSFULLY"
        response['status'] = "success"
        if request.is_ajax():
            return JsonResponse(response)
    return render(request, 'register.html')


# for get the ip address of client

def get_client_ip(request):
    # g = GeoIP() # for city X-Forwarded-For header, which contains a comma-separated list of IP addresses which

    # should be a list of all proxies
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        print("returning FORWARDED_FOR")
        ip = x_forwarded_for.split(',')[-1].strip()
    elif request.META.get('HTTP_X_REAL_IP'):
        print("returning REAL_IP")
        ip = request.META.get('HTTP_X_REAL_IP')
    else:
        print("returning REMOTE_ADDR")
        ip = request.META.get('REMOTE_ADDR')

        # city = g.city(ip)['city']  # for city

    return ip


def dashboard(request):
    email = request.session['username']['email']
    role = request.session['username']['role']
    userid = request.session['username']['userid']
    if role == 0:
        user = User.objects.all()
        context = {'user': user}
        return render(request, 'admindashboard.html', context)
    else:
        user = User.objects.filter(email=email).first()
        data = post.objects.filter(userid=userid)
        context = {'user': user,'data':data}
        return render(request, 'userdashboard.html', context)

def addpost(request):
    response = {}
    if request.method == 'POST':
        userssubmit = post()
        if request.FILES.get('image') is not None:
            userssubmit.image = request.FILES.get('image')
        userssubmit.userid =  request.session['username']['userid']
        userssubmit.save()
        response['message'] = "DATA SAVED SUCCESSFULLY"
        response['status'] = "success"
        if request.is_ajax():
            return JsonResponse(response)
    return render(request, 'post.html')

def viewpost(request,id):
    print("defrr")
    if request.method == 'POST':
        userssubmit = comments()
        userssubmit.comment = request.POST.get('comm')
        userssubmit.postid = request.POST.get('postid')
        userssubmit.userid = request.POST.get('userid')
        userssubmit.save()
    data = post.objects.filter(userid=id)
    data_list = []
    for item in data:
        data_list.append(item.id)
    comm = comments.objects.filter(postid__in=data_list)
    context = {'data': data,"comm":comm}
    return  render(request,'viewpost.html',context)


def userpost(request):
    userid = request.session['username']['userid']
    if request.method == 'POST':
        userssubmit = comments()
        userssubmit.comment = request.POST.get('comm')
        userssubmit.postid = request.POST.get('postid')
        userssubmit.userid = request.POST.get('userid')
        userssubmit.save()
    data = post.objects.filter(userid=userid)
    data_list = []
    for item in data:
        data_list.append(item.id)
    comm = comments.objects.filter(postid__in=data_list)
    context = {'data': data, "comm": comm}
    return render(request, 'user_post_view.html',context)
